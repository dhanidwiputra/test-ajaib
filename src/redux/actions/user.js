import { GET_USER_LIST } from '../types/user';
import axios from 'axios';

export function fetchData({ result, pageSize, page, search, gender, sortBy, sort }) {
  return (dispatch) => {
    let link = `https://randomuser.me/api`;
    let params = {
      results: pageSize,
      pageSize,
      page,
      search,
      gender,
      sortBy,
      sortOrder: sort
    };
    axios.get(link, { params })
      .then(response => {
        dispatch(setData(response.data.results));
      });
  };
}

export const setData = (payload) => ({
  type: GET_USER_LIST,
  payload
});