import { GET_USER_LIST } from '../types/user';

const initialState = {
  listUser: [],
};

function reducer(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_USER_LIST:
      return {
        ...state,
        listUser: payload
      };
    default:
      return state;
  }
}

export default reducer;