const styles = theme => ({
  filterContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 20
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  tableContainer: {
    marginTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    display: 'flex',
    flexDirection: 'column',
  },
  pagination: {
    width: "100%",
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  filterWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerContainer: {
    background: '#282c34',
    padding: '40px 20px',
    borderRadius: '0 0 40px 40px'
  },
  headerTitle: {
    margin: 0,
    textAlign: 'center',
    color: '#61dafb'
  },
  projectExplanation: {
    color: '#fff',
    textAlign: 'center'
  }
});

export default styles;
