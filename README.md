# Project Test Front End Engineer Ajaib

Project ini dibuat untuk mengikuti take home test proses rekrutmen ajaib dengan menggunakan create-react-app

## Library yang Dipakai
Project ini menggunakan beberapa library pihak ketiga seperti

#### 1. React Redux
#### 2. Redux-thunk
#### 3. React Material UI
## Persiapan Project

Untuk menjalankan project ini harap mengikuti beberapa script dibawah ini

### `npm install`

jalankan npm install terlebih dahulu agar menginstall semua library yang terdependensi dengan project ini, agar project bisa berjalan dengan baik.
### `npm start`

skrip ini akan menjalankan project ini pada browser anda secara otomatis, apabila tidak terbuka maka masukan pada kolom url dengan alamat [http://localhost:3000](http://localhost:3000) pada browsermu.


