import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextField, Container, Grid, InputAdornment, FormControl, Select, InputLabel, Button } from '@material-ui/core';
import SearchOutlined from '@material-ui/icons/SearchOutlined';
import Pagination from 'material-ui-flat-pagination';
import Table from '../../components/Table';

function debounce(func, wait) {
  let timeout;
  return function () {
    const context = this;
    const args = arguments;
    const later = function () {
      timeout = null;
      func.apply(context, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
}

function createDataListUser({
  name, email, login, gender, location
}) {
  return {
    username: login.username,
    name: name.first + ' ' + name.last,
    email,
    gender,
    country: location.country
  };
}

let columns = [
  { title: 'Username', field: 'username' },
  { title: 'Name', field: 'name' },
  { title: 'Email', field: 'email' },
  { title: 'Gender', field: 'gender' },
  { title: 'Country', field: 'country' },
];

class component extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sortBy: '',
      sort: '',
      gender: 'all',
      search: '',
      page: 1,
      pageSize: 10,
      result: 100
    };
  }

  componentDidMount() {
    this._fetchDataTable();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.page !== this.state.page ||
      prevState.gender !== this.state.gender ||
      prevState.sortBy !== this.state.sortBy ||
      prevState.sort !== this.state.sort
    ) {
      this._fetchDataTable();
    }
  }

  _changeSearch = (e) => {
    this.setState({
      search: e.target.value,
      page: 1
    });
    this._handleFetchSearch(e.target.value);
  }

  _handleFetchSearch = debounce(() => {
    this._fetchDataTable();
  }, 900);

  _fetchDataTable = () => {
    const { result, pageSize, page, search, gender, sortBy, sort } = this.state;
    this.props.actions.fetchData({ result, pageSize, page, search, gender, sortBy, sort });
  }

  _renderSearch = () => {
    return (
      <Grid item lg={6}>
        <TextField
          id="outlined-full-width"
          label="Search"
          style={{ margin: 8 }}
          placeholder="Search..."
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.search}
          onChange={this._changeSearch}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchOutlined />
              </InputAdornment>
            ),
          }}
          variant="outlined"
        />
      </Grid>
    );
  }

  _handleChangeGender = (e) => {
    this.setState({ gender: e.target.value });
  }

  _renderFilter = () => {
    const { classes } = this.props;
    const { gender } = this.state;
    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel htmlFor="outlined-age-native-simple">Gender</InputLabel>
        <Select
          native
          value={gender}
          onChange={this._handleChangeGender}
          label="Gender"
        >
          <option value={'all'}>All</option>
          <option value={'male'}>Male</option>
          <option value={'female'}>Female</option>
        </Select>
      </FormControl>
    );
  }

  _setDataList = () => {
    const { listUser } = this.props;
    return listUser.length ? listUser.map((item, index) =>
      createDataListUser({
        ...item
      })
    ) : [];
  };

  _handleClickPage = (page) => {
    this.setState({
      page: page
    })
  }

  _orderChange = (orderBy, orderDir) => {
    const { result, pageSize, page, search, gender, } = this.state;
    if (orderBy !== -1) {
      this.props.actions.fetchData({
        result, pageSize, page, search, gender,
        sortBy: columns[orderBy].field, sort: orderDir
      });
    }
  }

  _onChangeOrder = (order, orderBy) => {
    this.setState({
      sort: order,
      sortBy: orderBy
    })
  }

  _resetFilter = () => {
    this.setState({
      page: 1,
      search: '',
      gender: 'all'
    })
  }

  render() {
    const { classes } = this.props;
    const { pageSize, result, page } = this.state;

    const dataTable = this._setDataList();
    return (
      <div>
        <div className={classes.headerContainer}>
          <Container>
            <h1 className={classes.headerTitle}>Example Search and Filter Website</h1>
            <p className={classes.projectExplanation}>
              created with <span style={{color: '#61dafb'}}>React</span> by Dhany Dwi Putra
            </p>
          </Container>
        </div>
        <Container>
          <div className={classes.filterContainer}>
            {this._renderSearch()}
            <div className={classes.filterWrapper}>
              {this._renderFilter()}
              <Button onClick={() => this._resetFilter()} variant="outlined" color={'primary'}>Reset</Button>
            </div>
          </div>
          <div className={classes.tableContainer}>
            <Table data={dataTable} orderByChange={this._onChangeOrder} />
            <Pagination
              className={classes.pagination}
              limit={pageSize}
              offset={(page * pageSize) - pageSize}
              total={result}
              size={'large'}
              onClick={(e, off, page) => this._handleClickPage(page)}
            />
          </div>
        </Container>
      </div>
    );
  }
}

export default component;

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object.isRequired
};