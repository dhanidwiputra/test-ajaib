import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Component from './component';
import * as actions from '../../redux/actions/user';
import styles from './styles';
import { withStyles } from '@material-ui/core/styles';

function mapStateToProps(state) {
  const { listUser } = state.user;
  return {
    listUser
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

const Styled = withStyles(styles)(Component);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);
